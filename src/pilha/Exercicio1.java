package pilha;

public class Exercicio1 {

    public static int removerItemDaPila(int[] pilha, int numeroDado) {
        int topo = pilha.length - 1;
        int[] pilhaAux = new int[pilha.length];
        boolean estaVazia = pilha[topo] == -1;
        if (estaVazia) {
            throw new RuntimeException("pilha vazia");
        }
        for (int i = 0; i <= topo; i++) {
            if (numeroDado != pilha[i]) {
                pilhaAux[i] = pilha[i];
            }
        }
        imprimirPilha(pilhaAux);
        return pilha[topo];
    }

    public static void imprimirPilha(int[] pilha) {
        int topo = pilha.length - 1;
        for (int i = 0; i <= topo; i++) {
            System.out.println(pilha[i]);
        }
    }
}

package pilha;

class Pessoa {

    private String nome;
    private String telefone;
    private String endereco;

    public Pessoa(String nome, String idade, String endereco) {
        this.nome = nome;
        this.telefone = idade;
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdade() {
        return telefone;
    }

    public void setIdade(String idade) {
        this.telefone = idade;
    }

    @Override
    public String toString() {
        return "nome: " + nome + ", Telefone: " + telefone + ", endereço: " + endereco;
    }

}

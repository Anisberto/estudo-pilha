package pilha;

public class Exercicio3 {

    private int tamanho;
    private Pessoa[] array;
    private int topo = - 1;

    public Exercicio3(int tamanho) {
        if (tamanho > 0) {
            this.tamanho = tamanho;
            this.array = new Pessoa[tamanho];
        } else {
            throw new RuntimeException("Os valores devem ser maiores que zero") {
            };
        }
    }

    public Exercicio3() {
        this(10);
    }

    public void inserir(Pessoa pessoa) {
        if (estaCheia()) {
            throw new RuntimeException("Pilha cheia");
        }
        array[topo + 1] = pessoa;
        topo++;
    }

    public void retirar() {
        if (estaVazia()) {
            throw new RuntimeException("pilha vazia");
        }
        topo--;
    }

    public boolean estaVazia() {
        return topo == -1;
    }

    public boolean estaCheia() {
        return topo == (tamanho - 1);
    }

    public int quantidadeElementos() {
        return topo + 1;
    }

    public Pessoa consultar() {
        if (estaVazia()) {
            throw new RuntimeException("A pila esta vazia");
        }
        return array[topo];
    }

    public void esvaziar() {
        topo = -1;
    }

    public void imprimirPilha() {
        for (int i = 0; i <= topo; i++) {
            System.out.println(array[i]);
        }
    }
}

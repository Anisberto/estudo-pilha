package pilha;

import java.util.Scanner;

public class Aplication {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Iniciar: SIM ou N�O");
        String iniciarSystema = input.next().trim();
        while (iniciarSystema.equalsIgnoreCase("sim")) {
            System.out.println("informe a quest�o: 1 | 2 | 3 | 4 | 5");
            int process = input.nextInt();
            while (process < 0 || process >= 6) {
                System.out.println("informe uma op��o Valida!\nQuest�o: 1 | 2 | 3 | 4 | 5");
                process = input.nextInt();
            }
            switch (process) {
                case 1:
                    System.out.println("Vamos l� 1�: ");
                    Exercicio3 pilaE = new Exercicio3();

                    pilaE.inserir(new Pessoa("BERTO Pilhado", "uma pilha", "outra pilha"));
                    pilaE.inserir(new Pessoa("JOSE Pilhado", "uma pilha", "outra pilha"));
                    pilaE.inserir(new Pessoa("Vovo Pilhado", "uma pilha", "outra pilha"));
                    pilaE.inserir(new Pessoa("LUIZ Pilhado", "uma pilha", "outra pilha"));
                    pilaE.inserir(new Pessoa("JOB Pilhado", "uma pilha", "outra pilha"));

                    System.out.println("VALOR DO TOPO DA PILA: " + pilaE.consultar());
                    System.out.println("-------");
                    System.out.println("Desempilhando!");
                    pilaE.retirar();
                    System.out.println("-------");
                    System.out.println("VALOR DO TOPO DA PILA: " + pilaE.consultar());
                    System.out.println("-------");
                    System.out.println("Todos da Pilha");
                    pilaE.imprimirPilha();
                    System.out.println("-------");
                    System.out.println("VALOR DO TOPO DA PILA: " + pilaE.consultar());
                    break;
                case 2:
                    System.out.println("Vamos l� 2�: ");
                    int[] pil = new int[5];
                    pil[0] = 1;
                    pil[1] = 2;
                    pil[2] = 3;
                    pil[3] = 10;
                    pil[4] = 5;
                    System.out.println("Qual numero deseja retirar da Pilha? ");
                    int numeroDado = input.nextInt();
                    System.out.println("Nova pilha sem o numero (" + numeroDado + ")" + Exercicio1.removerItemDaPila(pil, numeroDado));
                    System.out.println("O Zero representa o numero retirado");
                    break;
                case 3:
                    System.out.println("Vamos l� 3�: ");
                    System.out.println("Informe um numero para ser convertido: ");
                    int numeroConverter = input.nextInt();
                    Exercicio2.lerNumeroEConverter(numeroConverter);
                    System.out.println("");
                    break;
                case 4:
                    System.out.println("Vamos l� 4�: ");
                    System.out.println("Ao Final da Opera��o o elemento do Topo sera o 'E'");
                    break;
                case 5:
                    System.out.println("Vamos l� 5�: ");
                    System.out.println("Informe um valor a ser verificado se � Palindro");
                    int valPalindro = input.nextInt();
                    System.out.print("E Palindro: ");
                    Exercicio5.verificarNumeroPalindro(valPalindro);
                    break;
                default:
                    System.out.println("Op��o Invalida!");
                    break;
            }
            System.out.println("Deseja Continuar: SIM ou N�O");
            iniciarSystema = input.next();
        }
        System.out.println("FIM!");
    }

}
